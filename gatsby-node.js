/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
exports.createPages = async ({actions, graphql, reporter}) => {
  const data = await graphql(`
    query {
      allDatoCmsRoom {
        nodes {
          slug
        }
      }
    }
  `);
  
  if (data.errors) {
    reporter.panic('No hubo resultados ',data.errors)
  }
  const habitaciones = data.data.allDatoCmsRoom.nodes;

  habitaciones.forEach(habitacion => {
    actions.createPage({
      path: habitacion.slug,
      component: require.resolve('./src/components/Habitacion.tsx'),
      context: {
        slug: habitacion.slug,
      }
    });
  });
};
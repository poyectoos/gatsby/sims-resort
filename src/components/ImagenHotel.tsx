import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import BackgroundImage from 'gatsby-background-image';
import styled from '@emotion/styled';

import useSeo from '../hooks/useSeo';

/*
==================================================
          Styled components
==================================================
*/
const Banner = styled(BackgroundImage)`
  height: 400px;
`;

const Claim = styled.div`
  background-image: linear-gradient(to top, rgba(34, 49, 63, 0.8), rgba(34, 49, 63, 0.8));
  color: #FFFFFF;
  height: 100%;
  display: flex;
  flex-direction: column;
  flex: 1;
  align-items: center;
  justify-content: center;
  text-align: center;

  h1 {
    font-size: 3rem;
    margin: 0;

    @media (min-width: 992px) {
      font-size: 5.8rem;
    }
  }

  p {
    font-size: 2rem;
  }
`;

const ImagenHotel = () => {
  const { siteName } = useSeo();

  const { image } = useStaticQuery(graphql`
    query {
      image: file (relativePath: { eq: "8.jpg" }) {
        sharp: childImageSharp {
          fluid(quality: 90, maxWidth: 1200) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `);

  return (
    <Banner
      Tag="section"
      fluid={image.sharp.fluid}
      fadeIn="soft"
    >
      <Claim>
        <h1>Bienvenido al { siteName }</h1>
        <p>El mejor { siteName } para tus vacaciones</p>
      </Claim>
    </Banner>
  );
};


export default ImagenHotel;
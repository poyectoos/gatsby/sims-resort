import React from 'react';
import Helmet from "react-helmet";
import { Global, css } from "@emotion/react";

import useSeo from '../hooks/useSeo';

import Header from "./Header";
import Footer from "./Footer";

//React.FC<PageProps<DataProps>>
const Layout = ({ children, page }) => {

  const { siteName, titleSuffix, fallbackSeo: { description, title } } = useSeo();
  

  return (
    <>
      <Global
        styles={
          css`
            :root {
              --primary: #002A32;
              --primary-lighten: #003540;
              --primary-darken: #00262E;
              --blue: #0d6efd;
              --indigo: #6610f2;
              --purple: #6f42c1;
              --pink: #d63384;
              --red: #dc3545;
              --orange: #fd7e14;
              --yellow: #ffc107;
              --green: #198754;
              --teal: #20c997;
              --cyan: #0dcaf0;
              --white: #fff;
              --gray: #6c757d;
              --gray-dark: #343a40;
            }
            html {
                font-size: 62.5%;
                box-sizing: border-box;
            }
            *, *:before, *:after {
                box-sizing: inherit;
            }
            body {
                font-size: 18px;
                font-size: 1.8rem;
                line-height: 1.5;
                font-family: 'PT Sans', sans-serif;
            }
            h1, h2, h3 {
                margin: 0;
                line-height: 1.5;
            }
            h1, h2 {
                font-family: 'Roboto', serif;
            }
            h3 {
                font-family: 'PT Sans', sans-serif;
            }
            ul {
                list-style: none;
                margin: 0;
                padding:0;
            }
            
            .btn {
              font-weight: 400;
              color: #FFFFFF;
              text-align: center;
              cursor: pointer;
              background-color: var(--primary);
              border: 1px solid transparent;
              padding: 0.8rem 1.2rem;
              border-radius: 0.5rem;
              text-decoration: none;
            }
            .btn:hover {
              color: #fff;
              background-color: var(--primary-darken);
              border-color: var(--primary-darken);
            }
            .btn-danger {
              color: #fff;
              background-color: #dc3545;
              border-color: #dc3545;
            }
            .btn-danger:hover {
              color: #fff;
              background-color: #bb2d3b;
              border-color: #b02a37;
            }
            .btn-success {
              color: #fff;
              background-color: #198754;
              border-color: #198754;
            }
            .btn-success:hover {
              color: #fff;
              background-color: #157347;
              border-color: #146c43;
            }
            .btn-warning {
              color: #000;
              background-color: #ffc107;
              border-color: #ffc107;
            }
            .btn-warning:hover {
              color: #000;
              background-color: #ffca2c;
              border-color: #ffc720;
            }
            .btn-info {
              color: #000;
              background-color: #0dcaf0;
              border-color: #0dcaf0;
            }
            .btn-info:hover {
              color: #000;
              background-color: #31d2f2;
              border-color: #25cff2;
            }
          `
        }
      />
      <Helmet>
        <title> { title } | { page }</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700|Roboto:400,700&display=swap" rel="stylesheet" />
        <meta
          name="description"
          content={description}
        />
      </Helmet>

      <Header title={title} />

      { children }

      <Footer title={ title } siteName={siteName} />
    </>
  );
};

export default Layout;
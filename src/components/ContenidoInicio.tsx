import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Image from 'gatsby-image';
import styled from '@emotion/styled';

const Titulo = styled.h2`
  text-align: center;
  font-size: 4rem;
  margin-top: 4rem;
`;

const Contenido = styled.div`
  padding-top: 2rem;
  padding-bottom: 2rem;
  max-width: 1000;
  width: 90%;
  margin: 0 auto;

  p {
    line-height: 2;
    margin-top: 1rem;
  }

  @media (min-width: 768px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    column-gap: 2rem;
  }

`;

const ContenidoInicio = () => {

  const { allDatoCmsPage: { nodes: [ pagina ] }} = useStaticQuery(
    graphql`
      query {
        allDatoCmsPage(filter: { slug: { eq: "inicio" } }) {
          nodes {
            slug
            titulo
            contenido
            imagen {
              fluid (maxWidth: 1200) {
                ...GatsbyDatoCmsFluid
              }
            }
          }
        }
      }
    `
  );

  const { slug, titulo, contenido, imagen: { fluid },  } = pagina;
  

  return (
    <>
      <Titulo>{ titulo }</Titulo>
      <Contenido>
        <p>{ contenido }</p>
        <Image fluid={ fluid } alt={ titulo } />
      </Contenido>
    </>
  );
};

export default ContenidoInicio;
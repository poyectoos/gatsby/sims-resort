import React from 'react';
import styled from "@emotion/styled";
import { Link } from 'gatsby';

import Navegation from './Navegation';

/*
==================================================
          Styled components
==================================================
*/
const FooterSC = styled.footer`
  background-color: var(--primary);
  padding: 1rem;
  margin-top: 4rem;
`;
const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;

  @media (min-width: 768px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;
const Brand = styled(Link)`
  padding: 0;
  margin: 0;
  color: #FFFFFF;
  text-align: center;
  font-size: 3rem;
  text-decoration: none;
`;
const Copy = styled.p`
  color: #FFFFFF;
`;

const Footer = ({ siteName, title }) => {

  const year = new Date().getFullYear();
  return (
    <FooterSC>
      <Container>
        <Navegation />
        <Brand to='/' >{ title }</Brand>
      </Container>
      <Container>
        <Copy>{ siteName } | Todos los derechos reservados &copy; { year }</Copy>
      </Container>
    </FooterSC>
  );
};

export default Footer;
import React from 'react';
import styled from '@emotion/styled';
import Image from 'gatsby-image';
import { Link } from 'gatsby';

const Habitacion = styled.li`
  border: 1px solid #e1e1e1;
  margin-bottom: 2rem;
`;
const Titulo = styled.h3`
`;
const Boton = styled(Link)`
  width: 100%;
`;
const Contenedor = styled.div`
  padding: 1.5rem 2rem;
`;

const PreviaHabitacion = ({ habitacion: { id, slug, titulo, contenido, imagen } }) => {

  return (
    <Habitacion>
      <Image fluid={ imagen } alt={ titulo } />
      <Contenedor>
        <Titulo>Habitaion { titulo }</Titulo>
        <p>{ contenido.slice(0, 100) }...</p>
        <Boton to={slug} className="btn" type="button">
          Ver informacion completa
        </Boton>
      </Contenedor>
    </Habitacion>
  );
};

export default PreviaHabitacion;
import React from 'react';
import { graphql } from 'gatsby';
import Image from 'gatsby-image';
import styled from '@emotion/styled';

import Layout from './Layout';

const Main = styled.main`
  margin: 0 auto;
  max-width: 1000px;
  width: 95%;
`;
const Titulo = styled.h1`
  text-align: center;
  font-size: 4rem;
  margin-top: 4rem; 
`;

export const query = graphql`
  query($slug: String!) {
    allDatoCmsRoom(filter: { slug: { eq: $slug } }) {
      nodes {
        titulo
        contenido
        imagen {
          fluid (maxWidth: 1200) {
            ...GatsbyDatoCmsFluid
          }
        }
        id
      }
    }
  }
`;

const Habitacion = ({ data: { allDatoCmsRoom: { nodes } } }) => {

  const { id, titulo, contenido, imagen: { fluid } } = nodes[0];

  return (
    <Layout page={ titulo }>
      <Main>
        <Titulo>Habitacion { titulo }</Titulo>
        <p>{ contenido }</p>
        <Image fluid={ fluid } alt={ titulo }  />
      </Main>
    </Layout>
  );
};

export default Habitacion;
import React from 'react';
import styled from "@emotion/styled";
import { Link } from 'gatsby';

import Navegation from './Navegation';

/*
==================================================
          Styled components
==================================================
*/
const HeaderSC = styled.header`
  background-color: var(--primary);
  padding: 1rem;
`;
const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;

  @media (min-width: 768px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;
const Brand = styled(Link)`
  padding: 0;
  margin: 0;
  color: #FFFFFF;
  text-align: center;
  font-size: 3rem;
  text-decoration: none;
`;

const Header = ({ title }) => {
  return (
    <HeaderSC>
      <Container>
        <Brand to='/' >{ title }</Brand>
        <Navegation />
      </Container>
    </HeaderSC>
  );
};

export default Header;
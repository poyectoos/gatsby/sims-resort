import { graphql, useStaticQuery } from 'gatsby';

import React from 'react';

const useRooms = () => {

  const { allDatoCmsRoom: { nodes }} = useStaticQuery(
    graphql`
      query {
        allDatoCmsRoom {
          nodes {
            id
            slug
            titulo
            contenido
            imagen {
              fluid (maxWidth: 1200) {
                ...GatsbyDatoCmsFluid
              }
            }
          }
        }
      }
    `
  );

  
  const habitaciones = nodes.map(habitacion => ({
    id: habitacion.id,
    slug: habitacion.slug,
    titulo: habitacion.titulo,
    contenido: habitacion.contenido,
    imagen: habitacion.imagen.fluid,
  }));
  

  return (habitaciones);
};

export default useRooms;
import { graphql, useStaticQuery } from 'gatsby';

const useSeo = () => {

  const { datoCmsSite: { globalSeo: { siteName, titleSuffix, fallbackSeo } } } = useStaticQuery(
    graphql`
      query {
        datoCmsSite {
          globalSeo {
            siteName,
            titleSuffix,
            fallbackSeo {
              title
              description
            }
          }
        }
      }
    `
  );

  return {
    siteName,
    titleSuffix,
    fallbackSeo
  };
};

export default useSeo;
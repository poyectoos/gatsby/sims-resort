import React from "react";
import Layout from "../components/Layout";
import styled from '@emotion/styled';

import ImagenHotel from '../components/ImagenHotel';
import ContenidoInicio from '../components/ContenidoInicio';
import PreviaHabitacion from '../components/PreviaHabitacion';

import useRooms from '../hooks/useRooms';

const Titulo = styled.h2`
  text-align: center;
  font-size: 4rem;
  margin-top: 4rem;
`;

const Galeria = styled.ul`
  max-width: 1000;
  width: 90%;
  margin: 0 auto;
  margin-top: 2rem;

  @media (min-width: 768px) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    column-gap: 3rem;
  }
`;

const IndexPage = () => {

  const habitaciones = useRooms();
  
  return (
    <Layout page="Inicio">
      <ImagenHotel />
      <ContenidoInicio />

      <Titulo>Nuestras habitaciones</Titulo>
        <Galeria>
          {
            habitaciones.map(habitacion => (
              <PreviaHabitacion
                key={habitacion.id}
                habitacion={habitacion}
              />
            ))
          }
        </Galeria>
    </Layout>
  );
}

export default IndexPage

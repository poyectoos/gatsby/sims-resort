import React from "react";

import Layout from "../components/Layout";
import ContenidoNosotros from "../components/ContenidoNosotros";


const Nosotros = () => {

   return (
    <Layout page="Inicio">
      <ContenidoNosotros />
    </Layout>
  );  
}
export default Nosotros
